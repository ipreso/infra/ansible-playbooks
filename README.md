Prerequisites
=============

In order to use some OVH API functions, you have to create an authentication token:
https://docs.ovh.com/gb/en/customer/first-steps-with-ovh-api/

Then, please create `/etc/ovh.conf` with the newly created credentials:

```
[default]
endpoint=ovh-eu

[ovh-eu]
application_key=XXX
application_secret=YYY
consumer_key=ZZZ
```

